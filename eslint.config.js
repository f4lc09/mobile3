module.exports = {
  languageOptions: {
    globals: {
      // Define your global variables here, if any
      // For example:
      // React Native globals
      __DEV__: true,
      // Node.js globals
      process: true,
      Buffer: true,
      setImmediate: true,
      clearImmediate: true,
    },
  },
  files: ['**/*.js', '**/*.jsx'],
  rules: {
    // Define your rules here
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
};
