import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import { CreationScreen } from '../Views/Creation';

test('creation process works correctly', async () => {
  const { getByPlaceholderText, getByText, queryByText } = render(<CreationScreen />);

  const input = getByPlaceholderText('Введите URL');
  fireEvent.changeText(input, 'https://jsonplaceholder.typicode.com/posts');

  const submitButton = getByText('Отправить');
  fireEvent.press(submitButton);

  const resultText = await queryByText('Создание завершено');
  expect(resultText).toBeTruthy();
});
