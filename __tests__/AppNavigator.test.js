import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import App from '../App';

test('navigates between tabs correctly', async () => {
  const { getByText, getByPlaceholderText } = render(<App />);

  expect(getByPlaceholderText('Введите URL')).toBeTruthy();

  const listTab = getByText('Список');
  fireEvent.press(listTab);

  await waitFor(() => {
    expect(getByText('Expected Text in ListScreen')).toBeTruthy();
  });
});
