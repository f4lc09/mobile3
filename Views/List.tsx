import { View, TouchableOpacity, Text, FlatList } from 'react-native';
import { styles } from './Styles';

const data = [
    { title: 'Плашка 1', key: '1' },
    { title: 'Плашка 2', key: '2' },
    { title: 'Плашка 3', key: '3' },
];

interface MaterialCardProps {
    title: string;
}

const MaterialCard: React.FC<MaterialCardProps> = ({ title }) => (
    <TouchableOpacity style={styles.card}>
        <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
);

export const ListScreen = () => (
    <View style={styles.container}>
        <FlatList data={data}
            style={styles.flatList}
            renderItem={({ item }) => (
                <MaterialCard title={item.title} />
            )}
            keyExtractor={item => item.key}
        />
    </View>
);
