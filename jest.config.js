module.exports = {
    preset: 'react-native',
    transformIgnorePatterns: [
        'node_modules/(?!(@react-native|react-native|@react-navigation|react-navigation-stack|react-navigation-tabs|react-native-gesture-handler|react-native-reanimated|react-native-screens|react-native-safe-area-context|react-native-safe-area-view|react-native-vector-icons|@react-native-community)/)'
    ],
    setupFilesAfterEnv: ['@testing-library/jest-native/extend-expect'],
    transform: {
        '^.+\\.[tj]sx?$': 'babel-jest',
    },
};
